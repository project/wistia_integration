INTRODUCTION
------------

Integrates the Wistia video service inside Drupal. This module intends to
integrate Wistia service such a way that user doesn't require to go to Wistia
website for management of Video projects, Uploads, media management, Video
stats etc.

Following functionalities are supported by the module
Wistia field, Uploader, configurations

Following features are planned in the future.
Wistia Project management
Wistia Media management
Project stats, Media stats

REQUIREMENTS
------------

This module do not have contrib dependencies.

INSTALLATION
------------

* Install as you would normally install a contributed Drupal module. Visit:
https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules

CONFIGURATION
-------------

* Go to /admin/config/wistia_integration/wistiasettings
* Add Wistia token and save configuration
* Now you can add wistia field to any configurable entity
